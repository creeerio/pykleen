PyKleen
=======

Input; simple, clean.

.. include:: example.inc.rst

API Reference
-------------

.. toctree::
  :maxdepth: 2

  tests
  structure
  development
